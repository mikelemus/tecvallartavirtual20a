<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_socialcomments', language 'es_mx', branch 'MOODLE_36_STABLE'
 *
 * @package   block_socialcomments
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actions'] = 'Acciones';
$string['activity'] = 'Actividad';
$string['allgroups'] = 'Todos los grupos';
$string['author'] = 'Autor';
$string['commentdeleted'] = 'Su comentario ha sido eliminado';
$string['commentscount'] = 'Comentarios: {$a}';
$string['commentsperpage'] = 'Comentarios por página';
$string['commentsperpagedesc'] = 'Configurar la cantidad de comentarios mostrados por página';
$string['content'] = 'Contenido';
$string['deletecheck'] = '¿Está seguro de querer eliminar esta publicación';
$string['digesttype'] = 'Tipos de resumen';
$string['eventreplycreated'] = 'Nueva  respuesta social';
$string['fromdate'] = 'De';
$string['haction'] = 'Acción';
$string['hactivity'] = 'Actividad / Recurso';
$string['hauthor'] = 'Autor';
$string['hcomment'] = 'Comentario';
$string['hcommentscount'] = 'Número de comentarios';
$string['hdate'] = 'Fecha y hora';
$string['htopicname'] = 'Nombre del tópico';
$string['newsfeed'] = 'Canal de noticias';
$string['pluginname'] = 'Comentarios Sociales';
$string['privacy:commentspath'] = 'Comentarios';
$string['privacy:metadata:block_socialcomments_cmmnts:content'] = 'El contenido de este comentario';
$string['privacy:metadata:block_socialcomments_replies:content'] = 'El contenido de esta respuesta.';
$string['privacy:repliespath'] = 'Respuestas';
$string['privacy:subscriptionspath'] = 'Suscripciones';
$string['reply'] = 'Respuesta';
$string['reportperpage'] = 'Ítems por página';
$string['reportspage'] = 'Reporte de comentarios';
$string['save'] = 'Guardar';
$string['selectactivity'] = 'Seleccionar actividad';
$string['selecttopic'] = 'Seleccionar tópico';
$string['socialcomments:deletecomments'] = 'Eliminar todos los comentarios';
$string['socialcomments:deleteowncomments'] = 'Eliminar comentarios propios';
$string['socialcomments:deleteownreplies'] = 'Eliminar respuestas propias';
$string['socialcomments:deletereplies'] = 'Eliminar todas las respuestas';
$string['socialcomments:postcomments'] = 'Publicar comentarios';
$string['socialcommentsreport'] = 'Reporte de comentarios sociales';
$string['socialcomments:subscribe'] = 'Suscribirse a bloque de comentarios';
$string['socialcomments:view'] = 'Ver comentarios';
$string['socialcomments:viewreport'] = 'Ver reporte';
$string['subscribed'] = 'Suscrito';
$string['taballcomments'] = 'Todos los comentarios';
$string['tabnewcomments'] = 'Nuevos comentarios';
$string['topic'] = 'Tópico';
$string['userspercron'] = 'Usuarios por cron';
$string['viewall'] = 'Ver todo';
