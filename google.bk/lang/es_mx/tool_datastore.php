<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_datastore', language 'es_mx', branch 'MOODLE_37_STABLE'
 *
 * @package   tool_datastore
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['datastoreenabled'] = 'Habilitar almacen de datos';
$string['datastoreenabled_desc'] = 'Permitir inicio de monitoreo de acciones del usuario en las tablas de almacenes de datos';
$string['datastorefieldscourse'] = 'Entidad de campos de curso a almacenar';
$string['datastorefieldscourser_desc'] = 'Ingresar nombres de campo separados con una coma (,).';
$string['datastorefieldsuser'] = 'Entidad de campos de usuario a almacenar';
$string['datastorefieldsuser_desc'] = 'Ingresar nombres de campo separados con una coma (,).';
$string['datastoresettings'] = 'Configuraciones de almacén de datos';
$string['pluginname'] = 'Almacén de datos';
$string['privacy:metadata:tool_datastore_action'] = 'Información acerca de acción del usuario, programa y usuario relacionado';
$string['privacy:metadata:tool_datastore:action'] = 'La acción a ser almacenada';
$string['privacy:metadata:tool_datastore:actionid'] = 'La acción relacionada';
$string['privacy:metadata:tool_datastore:data'] = 'Los datos de la entidad codificados como Json';
$string['privacy:metadata:tool_datastore_entity'] = 'Información acerca de las entidades relacionadas con la acción y la instantánea con los datos relacionados a cada una.';
$string['privacy:metadata:tool_datastore_entity:actionid'] = 'La acción ha sido almacenada';
$string['privacy:metadata:tool_datastore:entityid'] = 'La entidad relacionada';
$string['privacy:metadata:tool_datastore_entity:originalid'] = 'La ID, el curso, programa de la entidad principal, ...';
$string['privacy:metadata:tool_datastore_entity:snapshotid'] = 'La instantánea relacionada con todos los datos de la entidad, codificada como Json';
$string['privacy:metadata:tool_datastore_entity:type'] = 'El tipo de la entidad';
$string['privacy:metadata:tool_datastore_entity:usermodified'] = 'La ID del usuario que modificó por última vez la entrada de alguna forma.';
$string['privacy:metadata:tool_datastore:hash'] = 'El hash de los datos almacenados';
$string['privacy:metadata:tool_datastore_idx_fields'] = 'El valor de cada campo de entidad';
$string['privacy:metadata:tool_datastore_idx_fields:actionid'] = 'La ID de acción relacionada';
$string['privacy:metadata:tool_datastore_idx_fields:entityid'] = 'La ID de entidad relacionada';
$string['privacy:metadata:tool_datastore_idx_fields:name'] = 'El nombre de campo de la entidad';
$string['privacy:metadata:tool_datastore_idx_fields:usermodified'] = 'La ID del usuario que modificó por última vez el valor de alguna forma.';
$string['privacy:metadata:tool_datastore_idx_fields:value'] = 'El valor del campo';
$string['privacy:metadata:tool_datastore:name'] = 'El nombre del campo relacionado con la entidad';
$string['privacy:metadata:tool_datastore:originalcourseid'] = 'El curso relacionado con el usuario y la acción';
$string['privacy:metadata:tool_datastore:originalid'] = 'La ID original de la entidad';
$string['privacy:metadata:tool_datastore:originalprogramid'] = 'El programa relacionado con el curso, usuario y la acción';
$string['privacy:metadata:tool_datastore:relateduserid'] = 'La ID del usuario principal relacionado con la acción';
$string['privacy:metadata:tool_datastore_snapshot'] = 'Instantánea con los datos de cada entidad';
$string['privacy:metadata:tool_datastore_snapshot:data'] = 'Todos los datos relacionados con la entidad, codificados como Json';
$string['privacy:metadata:tool_datastore_snapshot:hash'] = 'Hash de los datos almacenados';
$string['privacy:metadata:tool_datastore:snapshotid'] = 'La instantánea relacionada con todos los datos relacionados con la entidad';
$string['privacy:metadata:tool_datastore_snapshot:usermodified'] = 'La ID del usuario que modificó por última vez los datos de alguna manera.';
$string['privacy:metadata:tool_datastore:type'] = 'El tipo de entidad como curso, programa, usuario ...';
$string['privacy:metadata:tool_datastore:usermodified'] = 'La ID del usuario que modificó por última vez la entrada de alguna forma.';
$string['privacy:metadata:tool_datastore:value'] = 'Programa de re-certificación';
$string['reg_wpdatastorerecords'] = 'Número de registros en datastore ({$a})';
