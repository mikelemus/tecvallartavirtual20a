<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_certificate', language 'es_mx', branch 'MOODLE_37_STABLE'
 *
 * @package   tool_certificate
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcertificate'] = 'Añadir plantilla de certificado';
$string['addcertificatedesc'] = 'Añadir nueva plantilla de certificado';
$string['addcertpage'] = 'Añadir página';
$string['addelement'] = 'Añadir elemento';
$string['aissueswerecreated'] = '{$a} emisiones fueron creadas';
$string['alignauto'] = 'Automático';
$string['aligncentre'] = 'Centro';
$string['alignleft'] = 'Izquierda';
$string['alignment'] = 'Alineación del texto';
$string['alignment_help'] = 'La alineación derecha del texto significará que las coordenadas del elemento (Posición X y Y) se referirán a la esquina superior derecha de la caja de texto, en alineación centrada se referirán a la parte central superior y en alineación izquierda a la esquina superior izquierda.';
$string['alignright'] = 'Derecha';
$string['awardedto'] = 'Otorgado a';
$string['cannotverifyallcertificates'] = 'Usted no tiene el permiso para verificar todos los certificados en el sitio.';
$string['certificate'] = 'Certificado';
$string['certificatecopy'] = '{$a} (copia)';
$string['certificate:imageforalltenants'] = 'Gestionar imágenes de certificados';
$string['certificateimages'] = 'Imágenes de certificados';
$string['certificate:issue'] = 'Emitir certificados para usuarios';
$string['certificate:issueforalltenants'] = 'Emitir certificado para usuarios en todos los inquilinos';
$string['certificate:manage'] = 'Gestionar certificados';
$string['certificate:manageforalltenants'] = 'Gestionar certificados en todos los inquilinos';
$string['certificates'] = 'Certificados';
$string['certificatesdescription'] = 'Estos son todos los certificados emitidos para la plantilla \'{$a}\'.';
$string['certificatesissued'] = 'Certificados emitidos';
$string['certificate:verify'] = 'Verificar cualquier certificado dentro del inquilino actual';
$string['certificate:verifyforalltenants'] = 'Verificar cualquier certificado en el sitio para todos los inquilinos';
$string['certificate:viewallcertificates'] = 'Verificar todas las plantillas de certificados y problemas para el inquilino actual';
$string['changeelementsequence'] = 'Traer al frente o mover atrás';
$string['code'] = 'Código';
$string['copy'] = 'Copiar';
$string['coursetimereq'] = 'Requerir minutos en curso';
$string['coursetimereq_help'] = 'Ingresar aquí la cantidad mínima de tiempo, en minutos, que un estudiante debe estar ingresado en el curso antes de que pueda recibir el certificado.';
$string['createtemplate'] = 'Nueva plantilla';
$string['deletecertpage'] = 'Eliminar página';
$string['deleteconfirm'] = 'Eliminar confirmación';
$string['deleteelement'] = 'Eliminar elemento';
$string['deleteelementconfirm'] = '¿Está seguro de querer eliminar el elemento \'{$a}\'?';
$string['deleteissueconfirm'] = '¿Está seguro de querer eliminar esta emisión de certificado?';
$string['deleteissuedcertificates'] = 'Eliminar certificados emitidos';
$string['deletepageconfirm'] = '¿Está seguro de querer eliminar esta página de certificado?';
$string['deletetemplateconfirm'] = '¿Está seguro de querer eliminar la plantilla de certificado \'{$a}\'  y todos sus datos asociados? Esta acción no puede deshacerse.';
$string['description'] = 'Descripción';
$string['duplicate'] = 'Duplicar';
$string['duplicateconfirm'] = 'Confirmación de duplicado';
$string['duplicateselecttenant'] = 'Seleccionar al inquilo para el cual duplicar en plantilla';
$string['duplicatetemplateconfirm'] = '¿Está seguro de querer duplicar la plantilla \'{$a}\'?';
$string['editcertificate'] = 'Editar certificado \'{$a}\\';
$string['editcontent'] = 'Editar contenido';
$string['editdetails'] = 'Editar detalles';
$string['editelement'] = 'Editar \'{$a}\\';
$string['editelementname'] = 'Editar nombre de elemento';
$string['editpage'] = 'Editar página {$a}';
$string['edittemplate'] = 'Editar plantilla';
$string['edittemplatename'] = 'Editar nombre de plantilla';
$string['elementname'] = 'Nombre del elemento';
$string['elementname_help'] = 'Este será el nombre usado para identificar este elemento al editar un certificado. Tenga en cuenta que esto no será mostrado en el PDF.';
$string['elementplugins'] = 'Plugins elemento';
$string['elements'] = 'Elemento';
$string['elements_help'] = 'Esta es la lista de elementos que serán mostrados en el certificado. Tenga en cuenta que los elementos son renderizados en este orden. El orden puede ser cambiado al usar las flechas junto a cada elemento.';
$string['elementwidth'] = 'Ancho, mm';
$string['elementwidth_help'] = 'Especificar el ancho del elemento. Cero (0) significa que no hay límite para el ancho.';
$string['entitycertificate'] = 'Certificado';
$string['entitycertificateissues'] = 'Emisiones de certificado';
$string['eventcertificateissued'] = 'Certificado emitido';
$string['eventcertificaterevoked'] = 'Certificado revocado';
$string['eventcertificateverified'] = 'Certificado verificado';
$string['eventtemplatecreated'] = 'Plantilla creada';
$string['eventtemplatedeleted'] = 'Plantilla eliminada';
$string['eventtemplateupdated'] = 'Plantilla actualizada';
$string['expired'] = 'Expirado';
$string['expires'] = 'Expira en';
$string['font'] = 'Font';
$string['fontcolour'] = 'Color';
$string['fontcolour_help'] = 'El color del font.';
$string['font_help'] = 'El font usado al generar este elemento.';
$string['fontsize'] = 'Tamaño en puntos';
$string['fontsize_help'] = 'El tamaño del font en puntos';
$string['getcertificate'] = 'Ver certificado';
$string['height'] = 'Altura, mm';
$string['hideshow'] = 'Ocultar/Mostrar';
$string['invalidcode'] = 'Código ingresado inválido.';
$string['invalidcolour'] = 'Color elegido inválido. Por favor ingrese un nombre de color  HTML válido, o un color hexadecimal de seis dígitos o tres dígitos.';
$string['invalidelementfortemplate'] = 'Elemento inválido para plantilla.';
$string['invalidelementwidth'] = 'Por favor ingrese un número positivo.';
$string['invalidheight'] = 'La altura debe ser un número válido mayor de 0.';
$string['invalidmargin'] = 'El margen debe ser un número válido mayor de 0.';
$string['invalidpagefortemplate'] = 'Página inválida para plantilla.';
$string['invalidposition'] = 'Por favor elija un número positivo para la posición {$a}.';
$string['invalidwidth'] = 'El ancho debe ser un número válido mayor de 0.';
$string['issuecertificates'] = 'Emitir nuevos certificados';
$string['issuedon'] = 'Emitido en';
$string['issuenewcertificate'] = 'Emitir nuevos certificados desde esta plantilla';
$string['issuenewcertificates'] = 'Emitir nuevos certificados';
$string['issuenotallowed'] = 'Usted no tiene permitido emitir certificados desde esta plantilla.';
$string['issueormangenotallowed'] = 'Usted no tiene permitido emitir certificados desde esta plantilla, ni gestionarla.';
$string['landscape'] = 'Apaisado';
$string['leftmargin'] = 'Margen izquierdo, mm';
$string['leftmargin_help'] = 'Este es el margen izquierdo del PDF del certificado en mm.';
$string['listofissues'] = 'Beneficiarios';
$string['load'] = 'Cargar';
$string['loadtemplate'] = 'Cargar plantilla';
$string['loadtemplatemsg'] = '¿Está seguro de querer cargar esta plantilla? Esto eliminará cualquier página y elementos existentes para este certificado.';
$string['manageelementplugins'] = 'Gestionar plugins de elementos de certificado';
$string['managetemplates'] = 'Gestionar plantillas de certificados';
$string['managetemplatesdesc'] = 'Este enlace lo llevará a una nueva pantalla en donde podrá gestionar plantillas usadas por la herramienta de certificados.';
$string['modify'] = 'Modificar';
$string['modulename'] = 'Herramienta de certificados';
$string['modulename_help'] = 'Este módulo permite la generación dinámica de certificados PDF.';
$string['modulenameplural'] = 'Herramienta de certificados';
$string['mycertificates'] = 'Mis certificados';
$string['mycertificatesdescription'] = 'Estos son los certificados que a Usted le han emitido, ya sea por Email o descargándolos manualmente.';
$string['name'] = 'Nombre';
$string['nametoolong'] = 'Usted ha excedido la longitud máxima permitida para el nombre';
$string['nocertificates'] = 'No hay certificados para este curso';
$string['noimage'] = 'Sin imagen';
$string['noissueswerecreated'] = 'No se crearon emisiones';
$string['norecipients'] = 'Sin beneficiarios';
$string['notemplates'] = 'Sin plantillas';
$string['notissued'] = 'No otorgado';
$string['notverified'] = 'No verificado';
$string['oneissuewascreated'] = 'Se creó una emisión';
$string['options'] = 'Opciones';
$string['outcomecertificate'] = 'Emitir certificado';
$string['outcomecertificatedescription'] = 'Emitir certificado \'{$a}\' a usuarios';
$string['page'] = 'Página {$a}';
$string['pageheight'] = 'Altura de página, mm';
$string['pageheight_help'] = 'Esta es la altura del PDF del certificado en mm. Para referencia, una hoja de papel A4 mide 297 mm de alto y una hoja carta mide 279mm de alto.';
$string['pagewidth'] = 'Ancho de página, mm';
$string['pagewidth_help'] = 'Este es el ancho del PDF del certificado en mm. Para referencia, una hoja de tamaño A4 tiene 210 mm de ancho y una hoja carta tiene 216 mm de ancho.';
$string['pluginadministration'] = 'Administración de herramienta certificado';
$string['pluginname'] = 'Herramienta de certificado';
$string['portrait'] = 'Retrato';
$string['posx'] = 'Posición X, mm';
$string['posx_help'] = 'Esta es la posición en mm desde la esquina superior izquierda donde Usted desea localizar el punto de referencia en la dirección X.';
$string['posy'] = 'Posición Y, mm';
$string['posy_help'] = 'Esta es la posición en mm desde la esquina superior izquierda donde Usted desea localizar el punto de referencia en la dirección Y.';
$string['print'] = 'Imprimir';
$string['privacy:metadata:tool_certificate:issues'] = 'La lista de certificados emitidos';
$string['privacy:metadata:tool_certificate_issues:code'] = 'El código que pertenece al certificado';
$string['privacy:metadata:tool_certificate_issues:expires'] = 'El sello de tiempo de cuando expira el certificado. 0 si no expira nunca.';
$string['privacy:metadata:tool_certificate_issues:templateid'] = 'La ID del certificado';
$string['privacy:metadata:tool_certificate_issues:timecreated'] = 'La hora de cuando fue emitido el certificado';
$string['privacy:metadata:tool_certificate_issues:userid'] = 'La ID del usuario a quien le fue emitido el certificado';
$string['rearrangeelements'] = 'Re-posicionar elementos';
$string['rearrangeelementsheading'] = 'Arrastrar y soltar elementos para cambiar en donde están posicionados en el certificado.';
$string['receiveddate'] = 'Otorgado en';
$string['refpoint'] = 'Localización de punto de referencia';
$string['refpoint_help'] = 'El punto de referencia es la localización de un elemento para el cual están determinadas sus coordenadas X y Y. Está indicado por el signo de \'+\' que aparece en el centro o en las esquinas del elemento.';
$string['reg_wpcertificates'] = 'Número de certificados ({$a})';
$string['reg_wpcertificatesissues'] = 'Número de certificados emitidos ({$a})';
$string['replacetemplate'] = 'Remplazar';
$string['requiredtimenotmet'] = 'Usted debe invertir al menos un mínimo de {$a->requiredtime} minutos en el curso antes de poder acceder a este certificado.';
$string['revoke'] = 'Revocar';
$string['revokecertificateconfirm'] = '¿Está seguro de querer revocar este certificado emitido de este usuario?';
$string['rightmargin'] = 'Margen derecho, mm';
$string['rightmargin_help'] = 'Este es el margen derecho del PDF del certificado en mm.';
$string['save'] = 'Guardar';
$string['saveandclose'] = 'Guardar y cerrar';
$string['saveandcontinue'] = 'Guardar y continuar';
$string['savechangespreview'] = 'Guardar cambios y previsualizar';
$string['savetemplate'] = 'Guardar plantilla';
$string['search:activity'] = 'Herramienta certificado - información de actividad';
$string['selectcertificate'] = 'Seleccionar certificado';
$string['selectedtenant'] = 'Inquilino seleccionado';
$string['selecttenant'] = 'Seleccionar inquilino';
$string['selectuserstoissuecertificatefor'] = 'Seleccionar usuarios para los cuales emitir certificado';
$string['settings'] = 'Configuraciones de herramienta certificado';
$string['shared'] = 'Compartido entre inquilinos';
$string['subplugintype_certificateelement_plural'] = 'Plugins Elemento';
$string['templatename'] = 'Nombre de plantilla';
$string['templatenameexists'] = 'El nombre de plantilla está actualmente en uso. Por favor elija un nombre diferente.';
$string['tenant'] = 'Inquilino';
$string['toomanycertificatestoshow'] = 'Demasiados certificados ({$a}) para mostrar';
$string['topcenter'] = 'Otorga';
$string['topleft'] = 'Superior izquierda';
$string['topright'] = 'Superior derecha';
$string['type'] = 'Tipo';
$string['uploadimage'] = 'Subir imagen';
$string['uploadimagedesc'] = 'Este enlace lo llevará a una nueva pantalla en donde podrá subir imágenes. Las imágenes subidas con este método estarán disponibles para todo el sitio para todos los usuarios que puedan crear un certificado.';
$string['valid'] = 'Válido';
$string['verified'] = 'Verificado';
$string['verify'] = 'Verificar';
$string['verifyallcertificates'] = 'Permitir verificación de todos los certificados';
$string['verifyallcertificates_desc'] = 'Cuando esta configuración está habilitada cualquier persona (incluyendo usuarios no ingresados al sitio) puede visitar el enlace \'{$a}\' ´para verificar cualquier certificado en el sitio, en lugar de tener que ir hasta el enlace de verificación para cada certificado. Tenga en cuenta que esto solamente aplica para certificados en donde \'Permitirle a cualquiera verificar un certificado\' esté configurado a \'Si\' en las configuraciones del certificado.';
$string['verifycertificate'] = 'Verificar certificado';
$string['verifycertificateanyone'] = 'Permitirle a cualquiera el verificar un certificado';
$string['verifycertificateanyone_help'] = 'Esta configuración habilita a cualquiera con  el enlace para verificación de certificación (incluyendo a usuarios no ingresados al sitio) para verificar un certificado.';
$string['verifycertificatedesc'] = 'Este enlace lo llevará a una nueva pantalla en donde podrá verificar certificados en el sitio.';
$string['verifycertificates'] = 'Verificar certificados';
$string['verifynotallowed'] = 'Usted no tiene permitido verificar certificados.';
$string['viewcertificate'] = 'Ver certificcado';
$string['width'] = 'Ancho, mm';
